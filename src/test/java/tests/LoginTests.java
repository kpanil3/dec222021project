package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.LoginPage;

public class LoginTests extends BaseTests{
	
	private LoginPage login ;
	@BeforeMethod
	public void setup() {
		login = new LoginPage();
	}
	
	
	
	@Test (dataProvider = "testUsers")
	public void checkLogin(String username, String password) {
		String homeTitle = login.enterUserName(username).enterPassword(password).clickLogin().pageTitle();
		Assert.assertEquals(homeTitle, "OrangeHRM");
	}
	
	@DataProvider
	public Object[][] testUsers() {
		Object[][] users = {{"admin", "admin123"}, {"abc", "abc"}};
		return users ;
	}

}
