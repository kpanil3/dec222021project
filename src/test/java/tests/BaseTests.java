package tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import driver.Driver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTests {
	
	static Properties prop;
	static FileInputStream fis;
	
	static {
		
	    try {
			fis = new FileInputStream(new File("src/test/resources/Configuration.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop = new Properties();
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@BeforeMethod
	public void setUp() {
		String browser = prop.getProperty("browser").toLowerCase();
		if (browser.equals("chrome")){
			WebDriverManager.chromedriver().setup();
			Driver.setDriver(new ChromeDriver());
			}
			else if (browser.equals("firefox")) {
				WebDriverManager.firefoxdriver().setup();
				Driver.setDriver(new FirefoxDriver());
			}
		Driver.getDriver().get(prop.getProperty("url"));
		Driver.getDriver().manage().window().maximize();
		Driver.getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
	
	@AfterMethod
	public void cleanUp() {
		
		Driver.getDriver().close();
		Driver.unload();
	}

}