package pages;

import org.openqa.selenium.By;
import driver.Driver;
public class LoginPage extends BasePage {
	
	private final By user = By.id("txtUsername");
	private final By passwd = By.id("txtPassword");
	private final By loginBtn = By.id("btnLogin");
	
	
    
    public LoginPage enterUserName(String uname) {
    	Driver.getDriver().findElement(user).sendKeys(uname);
    	return this;
    }
    
    
    public LoginPage enterPassword(String pwd) {
    	Driver.getDriver().findElement(passwd).sendKeys(pwd);
    	return this;
    }
    
    public HomePage clickLogin() {
    	Driver.getDriver().findElement(loginBtn).click();
     	return new HomePage();
    }

}
